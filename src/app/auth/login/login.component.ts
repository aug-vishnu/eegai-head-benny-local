import { CommonService } from './../../backend/common.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private commonService: CommonService, private router: Router) { }

  user: any;
  loggedIn: any;

  ngOnInit(): void {
    // this.authService.authState.subscribe((user) => {
    //   this.user = user;
    //   console.log(user);

    //   this.loggedIn = (user != null);
    // });
    this.DomFunc()
  }

  // /////////////////////////////////////////////////////////////////////////////////////////////
  DomFunc() {
    const signUpButton = document.getElementById('signUp');
    const signInButton = document.getElementById('signIn');
    const container = document.getElementById('container');

    signUpButton?.addEventListener('click', () => {
      container?.classList.add("right-panel-active");
    });

    signInButton?.addEventListener('click', () => {
      container?.classList.remove("right-panel-active");
    });

  }
  // /////////////////////////////////////////////////////////////////////////////////////////////
  // signInWithGoogle(): void {
  //   var user = this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
  //   console.log(user);


  // }

  // signOut(): void {
  //   this.authService.signOut();
  // }



  /////////////////////////////////////////////////////////////////////////////////////////////
  Creds: any = new Object()
  message: any;

  login(form: NgForm): void {
    this.message = null

    this.commonService.login_with_creds(form.value).subscribe(
      resp => {
        console.log(resp);
        try {
          localStorage.setItem('token', resp['token'])
        } catch (error) {
          Storage.set({ key: 'token', value: resp['token'] });
        }
        this.router.navigate(['']).then(() => { window.location.reload() })

      },
      error => {
        console.log('error', error);
        this.message = "Invalid Credentials"
      }
    );
  }

}
