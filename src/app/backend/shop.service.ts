import { Observable } from 'rxjs';
import { environment } from './../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Injectable({
  providedIn: 'root'
})
export class ShopService {

  constructor(private http: HttpClient) { }

  private APIurl = environment.APIurl + '/'
  private shop_id: any
  public shop: any = new Object()
  public config: any = []
  default_config = {
    'customer_database': [
      { 'field_name': 'Company name', 'status': true },
      { 'field_name': 'Contact Person', 'status': true },
      { 'field_name': 'Mobile Number', 'status': true },
      { 'field_name': 'Company place', 'status': true },
      { 'field_name': 'Customer Type', 'status': true },
      { 'field_name': 'Industry', 'status': true },
      { 'field_name': 'Calendar', 'status': true },
      { 'field_name': 'Options', 'status': true },
    ],
    'vendor_database': [
      { 'field_name': 'Company name', 'status': true },
      { 'field_name': 'Contact Person', 'status': true },
      { 'field_name': 'Mobile Number', 'status': true },
      { 'field_name': 'Company place', 'status': true },
      { 'field_name': 'Vendor Type', 'status': true },
      { 'field_name': 'Options', 'status': true },
    ],
    'user_database': [
      { 'field_name': 'Username', 'status': true },
      { 'field_name': 'Manager Name', 'status': true },
      { 'field_name': 'Manager Mobile', 'status': true },
      { 'field_name': 'Manager Address', 'status': true },
      { 'field_name': 'Options', 'status': true },
    ],
    'customer_followup': [
      { 'field_name': 'Company Name', 'status': true },
      { 'field_name': 'Mobile Number', 'status': true },
      { 'field_name': 'Last Feedback', 'status': true },
      { 'field_name': 'Last Follow Up', 'status': true },
      { 'field_name': 'Next Follow Up', 'status': true },
      { 'field_name': 'Priority', 'status': false },
      { 'field_name': 'Options', 'status': true },
    ],
    'payment_followup': [
      { 'field_name': 'Company Name', 'status': true },
      { 'field_name': 'Amount', 'status': true },
      { 'field_name': 'Next Follow Up', 'status': true },
      { 'field_name': 'Reference No.', 'status': true },
      { 'field_name': 'Last Feedback', 'status': true },
      { 'field_name': 'Last Follow Up', 'status': false },
      { 'field_name': 'Dispatch Date', 'status': false },
      { 'field_name': 'Options', 'status': true },
    ],
  }
  // Fetch shop ID for all Intercepts
  async get_shop_id() {
    const { value } = await Storage.get({ key: 'shop_id' });
    this.shop_id = value
  }


  ///////////////////////////////////////////////////////////////////////////////////////
  // Basic Lists Dump
  shop_detail(): Observable<any> {
    this.fetch_config()
    return this.http.post(this.APIurl + 'auth/shop-detail/', { ...this.shop })
  }
  set_shop_detail(params) {
    this.shop = params
    console.log("Inside Service" + this.shop.shop_name);
  }

  async fetch_config() {
    const { value } = await Storage.get({ key: 'config' });
    this.config = JSON.parse(value)
    console.log(this.config);
    if (this.config == null) {
      Storage.set({ key: 'config', value: JSON.stringify(this.default_config) });
      const { value } = await Storage.get({ key: 'config' });
      this.config = JSON.parse(value)
    }

  }
  ///////////////////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // Start Owner CRUD [ owner - auth ]
  owner_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'auth/owner-cud/', params)
  }

  owner_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'auth/owner-cud/', params)
  }

  owner_list(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/owner-list/', { ...this.shop })
  }

  owner_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        owner_id: params.owner_id
      },
    };

    return this.http.delete(this.APIurl + 'auth/owner-cud/', options)
  }

  // End Owner CRUD
  //////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // Start Manager CRUD [ manager - auth ]
  manager_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'auth/manager-cud/', params)
  }

  manager_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'auth/manager-cud/', params)
  }

  manager_list(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/manager-list/', { ...this.shop })
  }

  manager_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        manager_id: params.manager_id
      },
    };

    return this.http.delete(this.APIurl + 'auth/manager-cud/', options)
  }

  // End Manager CRUD
  //////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // Start Industry CRUD [ customer_industry - auth ]
  customer_industry_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'auth/customer-industry-cud/', params)
  }

  customer_industry_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'auth/customer-industry-cud/', params)
  }

  customer_industry_list(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/customer-industry-list/', { ...this.shop })
  }

  customer_industry_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        customer_industry_id: params.customer_industry_id
      },
    };

    return this.http.delete(this.APIurl + 'auth/customer-industry-cud/', options)
  }

  // End Industry CRUD
  //////////////////////////////////////////////////////////////////////////



  //////////////////////////////////////////////////////////////////////////
  // Start Customer CRUD [ customer - auth ]
  customer_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'auth/customer-cud/', params)
  }

  customer_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'auth/customer-cud/', params)
  }

  customer_list(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/customer-list/', { ...this.shop })
  }

  customer_detail(params): Observable<any> {
    console.log(params);

    return this.http.post(this.APIurl + 'auth/customer-detail/', { ...params, ...this.shop })
  }
  customer_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        customer_id: params.customer_id
      },
    };

    return this.http.delete(this.APIurl + 'auth/customer-cud/', options)
  }

  // End Customer CRUD
  //////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // Start Vendor CRUD [ vendor - auth ]
  vendor_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'auth/vendor-cud/', params)
  }

  vendor_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'auth/vendor-cud/', params)
  }

  vendor_list(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/vendor-list/', { ...this.shop })
  }

  vendor_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        vendor_id: params.vendor_id
      },
    };

    return this.http.delete(this.APIurl + 'auth/vendor-cud/', options)
  }

  // End Vendor CRUD
  //////////////////////////////////////////////////////////////////////////



  //////////////////////////////////////////////////////////////////////////
  // Start Machine CRUD [ machine - auth/ ]
  machine_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'auth/machine-cud/', params)
  }

  machine_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'auth/machine-cud/', params)
  }

  machine_list(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/machine-list/', { ...this.shop })
  }

  machine_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        machine_id: params.machine_id
      },
    };

    return this.http.delete(this.APIurl + 'auth/machine-cud/', options)
  }

  // End Machine CRUD
  //////////////////////////////////////////////////////////////////////////




  //////////////////////////////////////////////////////////////////////////
  // End of Auth Start of Core
  //////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  approval_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/approval-list/', { ...this.shop })
  }

  activity_list(): Observable<any> {
    return this.http.post(this.APIurl + 'journal/activity-list/', { ...this.shop })
  }
  //////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // Start Lead CRUD [ lead - core ]
  lead_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/lead-cud/', params)
  }

  lead_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'core/lead-cud/', params)
  }

  lead_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/lead-list/', { ...this.shop })
  }

  lead_filter(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/lead-filter/', params)
  }

  lead_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        lead_id: params.lead_id
      },
    };

    return this.http.delete(this.APIurl + 'core/lead-cud/', options)
  }

  // End Lead CRUD
  //////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // Start Invoice CRUD [ invoice - core ]
  invoice_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/invoice-cud/', params)
  }

  invoice_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'core/invoice-cud/', params)
  }

  invoice_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/invoice-list/', { ...this.shop })
  }

  invoice_filter(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/invoice-filter/', params)
  }

  invoice_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        invoice_id: params.invoice_id
      },
    };

    return this.http.delete(this.APIurl + 'core/invoice-cud/', options)
  }

  // End Invoice CRUD
  //////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // Start Task CRUD [ task - core ]
  task_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/task-cud/', params)
  }

  task_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'core/task-cud/', params)
  }

  task_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/task-list/', { ...this.shop })
  }

  task_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        task_id: params.task_id
      },
    };

    return this.http.delete(this.APIurl + 'core/task-cud/', options)
  }

  // End Task CRUD
  //////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // Start Task Statu CRUD [ task_status - core ]
  task_status_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/task-status-cud/', params)
  }

  task_status_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'core/task-status-cud/', params)
  }

  task_status_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/task-status-list/', { ...this.shop })
  }

  task_status_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        task_status_id: params.task_status_id
      },
    };

    return this.http.delete(this.APIurl + 'core/task-status-cud/', options)
  }

  // End Task Statu CRUD
  //////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // Start of Journal Start of Core
  //////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // Start Vendor CRUD [ journal - journal ]
  journal_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'journal/journal-cud/', params)
  }

  journal_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'journal/journal-cud/', params)
  }

  journal_response_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'journal/journal-response-cud/', params)
  }

  journal_list(): Observable<any> {
    return this.http.post(this.APIurl + 'journal/journal-list/', { ...this.shop })
  }

  journal_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        journal_id: params.journal_id
      },
    };

    return this.http.delete(this.APIurl + 'journal/journal-cud/', options)
  }

  // End Vendor CRUD
  //////////////////////////////////////////////////////////////////////////



  //////////////////////////////////////////////////////////////////////////
  // Start Vendor CRUD [ journal - journal ]
  notepad_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'journal/notepad-cud/', params)
  }

  notepad_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'journal/notepad-cud/', params)
  }

  notepad_list(): Observable<any> {
    return this.http.post(this.APIurl + 'journal/notepad-list/', { ...this.shop })
  }

  notepad_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        notepad_id: params.notepad_id
      },
    };

    return this.http.delete(this.APIurl + 'journal/notepad-cud/', options)
  }

  // End Vendor CRUD
  //////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////
  // End of Journal Start of Core
  //////////////////////////////////////////////////////////////////////////
}
