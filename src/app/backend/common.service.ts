import { Observable } from 'rxjs';
import { environment } from './../../environments/environment.prod';
import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


import { Plugins } from '@capacitor/core'
const { Storage } = Plugins


@Injectable({
  providedIn: 'root'
})
export class CommonService {



  // This http client will disable the interceptor and send non authenticated signals

  constructor(private http: HttpClient, handler: HttpBackend) {
    this.http = new HttpClient(handler);
  }

  // Variables
  private readonly APIurl = environment.APIurl + '/'
  public customer_id: any = ''
  public shop_id: any
  public shop: any = new Object()

  // Fetch shop ID for all Intercepts
  async get_shop_id() {
    const { value } = await Storage.get({ key: 'shop_id' });
    this.shop_id = value
    this.shop = { shop_id: this.shop_id }
  }

  // Basic Methods
  get_token() {
    try {
      return localStorage.getItem('token')
    } catch (error) {
      return Storage.get({ key: 'token' })
    }
  }

  async get_customer_id() {
    const { value } = await Storage.get({ key: 'customer_id' });
    this.customer_id = value
    console.log("Ser" + this.customer_id);
  }

  share_customer_id() {
    return this.customer_id
  }
  ///////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Essentials

  login_with_creds(credentials: any): Observable<any> {
    return this.http.post<any>(this.APIurl + "auth/login/", credentials)
  }

  loggedIn() {
    try {
      return !!localStorage.getItem('token')
    } catch (error) {
      return !!Storage.get({ key: 'token' })
    }
  }

  check_names(data: any) {
    return this.http.post<any>(this.APIurl + "allaccount/", data)
  }

  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////



  ///////////////////////////////////////////////////////////////////////////////////////
  // Basic Lists Dump
  shop_detail(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/shop-detail/', { ...this.shop })
  }
  ///////////////////////////////////////////////////////////////////////////////////////


}
