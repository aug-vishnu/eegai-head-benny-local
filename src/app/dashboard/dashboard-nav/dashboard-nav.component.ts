import { ShopService } from './../../backend/shop.service';
import { CommonService } from './../../backend/common.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-dashboard-nav',
  templateUrl: './dashboard-nav.component.html',
  styleUrls: ['./dashboard-nav.component.scss']
})
export class DashboardNavComponent implements OnInit {

  constructor(private router: Router, private shopService: ShopService, private commonService: CommonService) { }

  ngOnInit() {
    this.detail_shop()
  }


  isLoaded: any = false

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Shop Detail // Shop
  Shop: any;
  detail_shop() {
    this.shopService.shop_detail().subscribe(
      resp => {
        this.Shop = resp
        console.log(resp);
        this.isLoaded = true
        Storage.set({ key: 'site_id', value: resp['site_id'] });

        this.commonService.get_shop_id()
        this.shopService.get_shop_id()
        this.shopService.set_shop_detail(resp)
      }, error => {
        if (error.statusText == "Unauthorized") {
          console.log(error);
          this.logOut()
        }

      }
    )
  }

  // localStorage.setItem('user_type',this.Details.user_type)

  logOut() {
    try {
      localStorage.removeItem('token')
    } catch (error) {
      Storage.remove({ key: 'token' });
    }

    window.location.reload()
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

}
