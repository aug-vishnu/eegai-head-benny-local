import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ShopService } from './../../backend/shop.service';
import { CommonService } from './../../backend/common.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
declare var $: any;
@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.scss']
})
export class DashboardHomeComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) { }

  ngOnInit() {
    window.addEventListener('DOMContentLoaded', (event) => {
      console.log('DOM fully loaded and parsed');
    });
    /////////////////////////////////////////////////////////////////////////////////////////////
    this.detail_shop()
    this.list_leads()
    this.list_tasks()
    this.list_invoices()
    /////////////////////////////////////////////////////////////////////////////////////////////
  }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Date Operations
  PRIORITY: any = [{ id: 1, value: 'High' }, { id: 2, value: 'Normal' }, { id: 3, value: 'Low' }]
  CUSTOMER_FROM: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Local A' }, { id: 2, value: 'Local B' }, { id: 3, value: 'Outstation A' }, { id: 4, value: 'Outstation B' }, { id: 5, value: 'Marketing' }]
  isFollowUp = false

  active_day = new Date()
  selected_date = new Date();
  isLoading = false
  isExpand = false
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Shop Detail // Shop
  Shop: any;
  detail_shop() {
    this.shopService.shop_detail().subscribe(
      resp => {
        this.Shop = this.shopService.shop
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start List Sections //
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Leads Detail // Leads
  Leads: any;
  Lead: any = new Object();
  list_leads() {
    this.isLoading = true
    this.Lead.filter_type = '1'
    this.Lead.filter_date_one = this.DateTimetoDay(this.active_day)
    this.shopService.lead_filter(this.Lead).subscribe(
      resp => {
        this.Leads = resp
        console.log(resp);
        this.rerender()

      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Invoices Detail // Invoices
  Invoices: any;
  Invoice: any = new Object();
  list_invoices() {
    this.Invoice.filter_type = '1'
    this.Invoice.filter_date_one = this.DateTimetoDay(this.active_day)
    this.shopService.invoice_list().subscribe(
      resp => {
        this.Invoices = resp
        console.log("Ohoooooooooooooooooo"+resp);
        this.rerender2()
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Tasks Detail // Task
  Tasks: any;
  Customers: any;
  Task: any = new Object();
  list_tasks() {
    this.shopService.task_list().subscribe(
      resp => {
        this.Tasks = resp
        console.log(resp);
      })
      this.shopService.customer_list().subscribe(
        resp => {
          this.Customers = resp
          console.log(resp);
          this.isLoading = false
        })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // End List Sections //
  /////////////////////////////////////////////////////////////////////////////////////////////




  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Disclose Sections //
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Leads Detail // Leads
    /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ lead ]
  edit_factor_lead(form: NgForm) {
    this.Lead.lead_priority = 2 // Here
    this.Lead.next_follow_up = this.DateTimetoDay(this.selected_date) + 'T00:00'
    console.log(this.Lead);
    console.log(this.selected_date);

    this.shopService.lead_create(this.Lead).subscribe(
      resp => {
        console.log(resp)
        this.list_leads()
        if (this.isFollowUp) {
          console.log(this.Lead);
          this.disclose_lead(this.Lead)
          this.isFollowUp = false
        }
        this.Lead = new Object();
        $('#create-lead-modal').modal('hide');
        form.reset()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  next_followup_lead(ele) {
    $('#create-lead-modal').modal('show')
    this.isFollowUp = true
    this.Lead.lead_id = ele.lead_id
    this.Lead.customer = ele.customer_id
    // this.Lead = ele
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  disclose_lead(ele) {
    // this.Lead = new Object()
    this.Lead.lead_id = ele.lead_id
    this.Lead.customer = ele.customer_id
    this.Lead.dislosed_state = true
    this.shopService.lead_edit(this.Lead).subscribe(
      resp => {
        console.log(resp)
        this.list_leads()
        this.Lead = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  disclose_task(ele) {
    this.Task = new Object()
    this.Task.task_id = ele.task_id
    this.Task.dislosed_state = true
    this.shopService.task_edit(this.Task).subscribe(
      resp => {
        console.log(resp)
        this.list_tasks()
        this.Task = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  disclose(ele) {
    this.Invoice = new Object()
    this.Invoice.invoice_id = ele.invoice_id
    this.Invoice.dislosed_state = true
    this.shopService.invoice_edit(this.Invoice).subscribe(
      resp => {
        console.log(resp)
        this.list_invoices()
        this.Invoice = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  togglePanels() {
    if (this.isExpand) { this.accordion.closeAll() }
    else { this.accordion.openAll() }
    this.isExpand = !this.isExpand
  }
  onDateChange(e) {
    console.log(this.active_day);
    this.active_day = e.target.value
    this.list_leads()
    this.list_tasks()
    this.list_invoices()
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Status [ task ]
  Task_status: any = new Object()
  add_status(form: NgForm, factor) {
    this.Task_status.task = factor.task_id
    this.Task_status.task_state = 1
    this.Task_status.task_status = form.value.task_status_name

    this.shopService.task_status_create(this.Task_status).subscribe(
      resp => {
        console.log(resp)
        this.list_tasks()
        this.Task_status = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Status
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // End Disclose Sections //
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Timer Functions
  // Input : Raw DateTime // Output : dd-mm-yy
  DateTimetoDay(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////
  isCustomerSelected: any = false
  Customer: any = new Object();
  show_detail(ele: any) {
    console.log(ele);
    Storage.set({ key: 'customer_id', value: ele.customer_id });
    this.Customer.customer_id = ele.customer_id
    this.ngOnInit()
    this.isCustomerSelected = true
    // this.isCustomerSelected = false
    this.detail_customer()
    $('#customer-detail-modal').modal('show')
  }

  detail_customer() {
    this.shopService.customer_detail(this.Customer).subscribe(
      // this.Customer = new Object()
      resp => {
        this.Customer = resp
        console.log(resp);
        this.Customer["leads"] = this.Customer["leads"].reverse()
        this.Customer["invoices"] = this.Customer["invoices"].reverse()
        this.Customer["tasks"] = this.Customer["tasks"].reverse()
        this.isLoading = false
      })
  }
  add_lead(){
    $('#customer-detail-modal').modal('hide')
    $('#create-lead-modal').modal('show');
    this.Lead.customer = this.Customer.customer_id
    // this.router.navigate(['/dashboard/lead-management'], {state: {flag: 1 , customer: this.Customer }});
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtEle2: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtTrigger2: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    responsive: true,
    ordering: false
  };

  ngAfterViewInit(): void {
    this.dtTrigger.next();

    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
        const that = this;
        $('input', this.footer()).on('keyup change', function () {
          if (that.search() !== this['value']) {
            that
              .search(this['value'])
              .draw();
          }
        });
      });
    });
  }
  // DataTables Functions
  // ngAfterViewInit(): void {
  //   this.dtTrigger.next();
  // }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  rerender2(): void {
    this.dtEle2.dtInstance.then((dtInstance2: DataTables.Api) => {
      // Destroy the table first
      dtInstance2.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger2.next();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

}
