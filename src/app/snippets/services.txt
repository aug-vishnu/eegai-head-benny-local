
  constructor(private http: HttpClient) { }

  private APIurl = environment.APIurl + '/'
  private shop_id = environment.shop_id
  private shop = { shop_id: this.shop_id }

  //////////////////////////////////////////////////////////////////////////
  // Start Factor CRUD [ factor ]
  factor_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/factor-cud/', params)
  }

  factor_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'core/factor-cud/', params)
  }

  factor_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/factor-list/', { ...this.shop })
  }

  factor_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        shop_id: this.shop_id,
        factor_id: params.factor_id
      },
    };

    return this.http.delete(this.APIurl + 'core/factor-cud/', options)
  }

  // End Factor CRUD
  //////////////////////////////////////////////////////////////////////////
