import { NotepadManagementComponent } from './eegai/notepad-management/notepad-management.component';
import { ApprovalManagementComponent } from './eegai/approval-management/approval-management.component';
import { UserManagementComponent } from './eegai/user-management/user-management.component';
import { JournalManagementComponent } from './eegai/journal-management/journal-management.component';
import { VendorManagementComponent } from './eegai/vendor-management/vendor-management.component';
import { ManagerManagementComponent } from './eegai/manager-management/manager-management.component';
import { OwnerManagementComponent } from './eegai/owner-management/owner-management.component';
import { LeadManagementComponent } from './eegai/lead-management/lead-management.component';
import { TaskManagementComponent } from './eegai/task-management/task-management.component';
import { DashboardHomeComponent } from './dashboard/dashboard-home/dashboard-home.component';
import { AuthGuard } from './backend/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { DashboardNavComponent } from './dashboard/dashboard-nav/dashboard-nav.component';
import { ShopProfileComponent } from './eegai/shop-profile/shop-profile.component';
import { InvoiceManagementComponent } from './eegai/invoice-management/invoice-management.component';
import { CustomerManagementComponent } from './eegai/customer-management/customer-management.component';
import { CustomerDetailComponent } from './eegai/customer-detail/customer-detail.component';


const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full', },

  // Auth
  { path: 'signin', component: LoginComponent },
  // { path:'signup', component: RegisterComponent },


  // Admin-Panel
  {
    path: 'dashboard',
    component: DashboardNavComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: DashboardHomeComponent },
      // { path: '', component: ArambHomeComponent },

      // Master DataBases
      { path: 'customer-management', component: CustomerManagementComponent },
      { path: 'customer-detail', component: CustomerDetailComponent },
      { path: 'vendor-management', component: VendorManagementComponent },
      { path: 'user-management', component: UserManagementComponent },

      // Follow Ups
      { path: 'lead-management', component: LeadManagementComponent },
      { path: 'task-management', component: TaskManagementComponent },
      { path: 'invoice-management', component: InvoiceManagementComponent },

      { path: 'approval-management', component: ApprovalManagementComponent },

      // Journal
      { path: 'journal-management', component: JournalManagementComponent },
      { path: 'notepad-management', component: NotepadManagementComponent },

      // Users
      { path: 'owner-management', component: OwnerManagementComponent },
      { path: 'manager-management', component: ManagerManagementComponent },

      // Shop Management
      { path: 'shop-management', component: ShopProfileComponent },
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
