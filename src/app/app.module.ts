import { CustomerManagementComponent } from './eegai/customer-management/customer-management.component';
import { ShopService } from './backend/shop.service';
import { AuthGuard } from './backend/auth.guard';
import { CommonService } from './backend/common.service';
import { InterceptorService } from './backend/interceptor.service';
import { LoginComponent } from './auth/login/login.component';
import { DashboardHomeComponent } from './dashboard/dashboard-home/dashboard-home.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardNavComponent } from './dashboard/dashboard-nav/dashboard-nav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataTablesModule } from 'angular-datatables';
import { ToastrModule } from 'ngx-toastr';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// For Google OAuth
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { LeadManagementComponent } from './eegai/lead-management/lead-management.component';
import { TaskManagementComponent } from './eegai/task-management/task-management.component';
import { InvoiceManagementComponent } from './eegai/invoice-management/invoice-management.component';
import { ManagerManagementComponent } from './eegai/manager-management/manager-management.component';
import { OwnerManagementComponent } from './eegai/owner-management/owner-management.component';
import { ShopProfileComponent } from './eegai/shop-profile/shop-profile.component';

// Material Imports
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule, MatFormFieldControl } from '@angular/material/form-field';

import { JournalManagementComponent } from './eegai/journal-management/journal-management.component';
import { VendorManagementComponent } from './eegai/vendor-management/vendor-management.component';
import { CustomerDetailComponent } from './eegai/customer-detail/customer-detail.component';
import { UserManagementComponent } from './eegai/user-management/user-management.component';
import { ApprovalManagementComponent } from './eegai/approval-management/approval-management.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { NotepadManagementComponent } from './eegai/notepad-management/notepad-management.component';
import { SideBarComponent } from './dashboard/side-bar/side-bar.component';
import { TopBarComponent } from './dashboard/top-bar/top-bar.component';
import { RightBarComponent } from './dashboard/right-bar/right-bar.component';
import { DashboardFooterComponent } from './dashboard/dashboard-footer/dashboard-footer.component';



@NgModule({
  declarations: [
    AppComponent,
    DashboardHomeComponent,
    DashboardNavComponent,
    LoginComponent,
    CustomerManagementComponent,
    LeadManagementComponent,
    TaskManagementComponent,
    InvoiceManagementComponent,
    ManagerManagementComponent,
    OwnerManagementComponent,
    ShopProfileComponent,
    JournalManagementComponent,
    VendorManagementComponent,
    CustomerDetailComponent,
    UserManagementComponent,
    ApprovalManagementComponent,
    NotepadManagementComponent,
    SideBarComponent,
    TopBarComponent,
    RightBarComponent,
    DashboardFooterComponent,],
  imports: [

    // Basic imports for all ithalam kandipa irukanum
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule, // Animations Uhhh
    ToastrModule.forRoot(), // for pop up notification
    HttpClientModule, // to fetch with backend
    FormsModule, // Normal Form
    // ReactiveFormsModule, // for image forms
    DataTablesModule, // All tables
    // MDBBootstrapModule.forRoot(), // Advanced Bootstrap
    AppRoutingModule, // app.routing
    // npm i @ng-select/ng-select
    NgSelectModule,


    // Material Components
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatCardModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatListModule,
    MatInputModule,
    MatDividerModule,
    MatTabsModule,
    MatFormFieldModule,
    MatIconModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),

  ],
  providers: [
    ShopService,
    CommonService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
    },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

/*
** Check whether you have angular version 10.2.1
ng version

** If not
npm uninstall -g angular-cli
npm cache clean
npm install -g angular-cli@10.2.1

** Install This Afterwards
npm install -D @angular/cdk ngx-toastr bootstrap @ng-select/ng-select @capacitor/core @capacitor/cli angularx-social-login jquery popper.js chart.js font-awesome hammerjs animate.css datatables.net angular-bootstrap-md datatables.net-dt angular-datatables @types/jquery @types/datatables.net --save

** Change contents of All Format DATE : 3-2-21

 */











