import { CommonService } from './../../backend/common.service';
import { MatAccordion } from '@angular/material/expansion';
import { ShopService } from './../../backend/shop.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
declare var $: any;

import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-customer-management',
  templateUrl: './customer-management.component.html',
  styleUrls: ['./customer-management.component.scss']
})
export class CustomerManagementComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private shopService: ShopService, private commonService: CommonService,private router: Router) { }
  customer_id: any

  ngOnInit(): void {
    this.isLoading = true
    this.config = this.shopService.config

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    this.list_industries()
    /////////////////////////////////////////////////////////////////////////////////////////////
    // this.commonService.get_customer_id().then(() => {
    //   this.customer_id = this.commonService.share_customer_id()
    //   this.Customer.customer_id = this.customer_id
    //   console.log(this.customer_id);
    //   this.detail_customer()
    // })
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Variables
  CUSTOMER_FROM: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Local_A' }, { id: 2, value: 'Local_B' }, { id: 3, value: 'Outstation A' }, { id: 4, value: 'Outstation B' }, { id: 5, value: 'Marketing' }]

  config: any;
  selected_date = new Date();
  isLoading: any
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products [ customer ]
  Factors: any = [];
  list_factor() {
    this.shopService.customer_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.rerender()
      }
    )
  }

  Industries: any = [];
  selectedIndustry: any = null;
  list_industries() {
    this.shopService.customer_industry_list().subscribe(
      resp => {
        this.isLoading = false
        this.Industries = resp
        console.log(resp);
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ customer ]
  Factor: any = new Object()
  create_factor(form: NgForm) {
    // this.Factor.shop_industry = this.selectedIndustry
    // this.Factor.customer_type = this.selectedType
    // this.Factor.customer_from = this.selectedFrom
    this.Factor.customer_from = 1
    // this.Factor.customer_type = 2
    this.Factor.customer_gstn = null
    this.Factor.customer_feedback = null
    form.value.calendar_eligible == undefined ? this.Factor.calendar_eligible = false : this.Factor.calendar_eligible = true
    form.value.customer_direct == undefined ? this.Factor.customer_direct = false : this.Factor.customer_direct = true

    console.log(this.Factor);
    this.shopService.customer_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        $('#create-factor-modal').modal('hide');
        form.reset()
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Status [ task ]
  Industry: any = new Object()
  add_industry(form: NgForm) {

    this.shopService.customer_industry_create(form.value).subscribe(
      resp => {
        console.log(resp)
        this.list_industries()
        $('#create-industry-modal').modal('hide');
        this.Industry = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Status
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ customer ]
  edit_factor(form: NgForm) {
    console.log(form.value);
    this.Factor.shop_industry = this.Factor.customer_industry_id
    // this.Factor.customer_type = form.value.customer_type
    // this.Factor.customer_from = form.value.customer_from
    this.Factor.customer_from = 1
    this.Factor.customer_gstn = null
    // this.Factor.customer_type = 2
    // form.value.calendar_eligible == undefined ? this.Factor.calendar_eligible = false : this.Factor.calendar_eligible = true


    this.shopService.customer_edit(this.Factor).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        $('#edit-factor-modal').modal('hide');
        form.reset()
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_factor(ele: any) {
    this.Factor.customer_id = ele.customer_id
    this.shopService.customer_delete(this.Factor).subscribe(
      resp => {
        this.list_factor()
        $('#edit-factor-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_industry(ele: any) {
    this.Industry.customer_industry_id = ele.customer_industry_id
    this.shopService.customer_industry_delete(this.Industry).subscribe(
      resp => {
        this.list_factor()
        this.list_industries()
        $('#industry-create-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  show_edit(ele: any) {
    console.log(ele);
    this.Factor = ele
    $('#edit-factor-modal').modal('show')
  }

  isCustomerSelected: any = false
  Customer: any = new Object();
  show_detail(ele: any) {
    console.log(ele);
    this.Customer = new Object();
    Storage.set({ key: 'customer_id', value: ele.customer_id });
    this.Customer.customer_id = ele.customer_id
    // this.ngOnInit()
    this.isCustomerSelected = true
    // this.isCustomerSelected = false
    this.detail_customer()
    $('#customer-detail-modal').modal('show')
  }

  detail_customer() {
    this.shopService.customer_detail(this.Customer).subscribe(
      // this.Customer = new Object()
      resp => {
        this.Customer = resp
        console.log(resp);
        this.Customer["leads"] = this.Customer["leads"].reverse()
        this.Customer["invoices"] = this.Customer["invoices"].reverse()
        this.Customer["tasks"] = this.Customer["tasks"].reverse()
        this.isLoading = false
      })
  }
    /////////////////////////////////////////////////////////////////////////////////////////////
    isExpand: any = false
    togglePanels() {
      if (this.isExpand) { this.accordion.closeAll() }
      else { this.accordion.openAll() }
      this.isExpand = !this.isExpand
    }
  /////////////////////////////////////////////////////////////////////////////////////////////
  add_lead(){
    $('#customer-detail-modal').modal('hide')
    $('#create-lead-modal').modal('show');
    this.Lead.customer = this.Customer.customer_id
    // this.router.navigate(['/dashboard/lead-management'], {state: {flag: 1 , customer: this.Customer }});
  }
  add_payment(){
    $('#customer-detail-modal').modal('hide')
    $('#create-lead-modal').modal('show');
    // this.router.navigate(['/dashboard/invoice-management'], {state: {flag: 1 , customer: this.Customer }});
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    // responsive: true,
    pageLength: 100000,
    // ordering: false
  };

  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
        const that = this;
        $('input', this.footer()).on('keyup change', function () {
          if (that.search() !== this['value']) {
            that
              .search(this['value'])
              .draw();
          }
        });
      });
    });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  // config = this.shopService.config
  update_config() {
    Storage.set({ key: 'config', value: JSON.stringify(this.config) });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Timer Functions
  // Input : Raw DateTime // Output : dd-mm-yy
  DateTimetoDay(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ lead ]
  Lead: any = new Object()
  create_lead(form: NgForm) {

    this.Lead.lead_priority = 2 // Here
    this.Lead.next_follow_up = this.DateTimetoDay(this.selected_date) + 'T00:00'
    console.log(this.Lead);
    console.log(this.selected_date);

    this.shopService.lead_create(this.Lead).subscribe(
      resp => {
        console.log(resp)
        this.Lead = new Object();
        $('#create-lead-modal').modal('hide');
        form.reset()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////

}
