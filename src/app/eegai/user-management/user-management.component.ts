import { ShopService } from './../../backend/shop.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
declare var $: any;

import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {


  constructor(private shopService: ShopService, private router: Router) { }

  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  config = this.shopService.config
  isLoading: any = false

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products [ customer ]
  Factors: any = [];
  list_factor() {
    this.isLoading = true
    this.shopService.manager_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.isLoading = false
        this.rerender()
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ customer ]
  Factor: any = new Object()
  create_factor(form: NgForm) {
    this.shopService.manager_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor.manager_id = resp.manager_id
        // this.Factor = new Object()
        $('#create-factor-modal').modal('hide');
        form.reset()
        this.shopService.manager_edit(this.Factor).subscribe(
          resp => {
            console.log("Edited " + resp);

          }
        )
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ customer ]
  edit_factor(form: NgForm) {

    this.shopService.manager_edit(this.Factor).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_factor(ele: any) {
    this.Factor = ele
    this.shopService.manager_delete(this.Factor).subscribe(
      resp => {
        this.list_factor()
        $('#edit-factor-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  show_edit(ele: any) {
    console.log(ele);
    this.Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    // responsive: true,
    // ordering: false
  };

  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  update_config() {
    Storage.set({ key: 'config', value: JSON.stringify(this.config) });
  }

  /////////////////////////////////////////////////////////////////////////////////////////////

}
