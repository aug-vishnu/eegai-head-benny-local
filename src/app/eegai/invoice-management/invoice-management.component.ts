import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { NgForm } from '@angular/forms';
import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-invoice-management',
  templateUrl: './invoice-management.component.html',
  styleUrls: ['./invoice-management.component.scss']
})
export class InvoiceManagementComponent implements OnInit {

  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) { }

  ngOnInit() {
    this.isLoading = true

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.detail_shop()
    this.list_invoices()
    this.list_customers()
    /////////////////////////////////////////////////////////////////////////////////////////////
  }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Date Operations
  CUSTOMER_FROM: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Local A' }, { id: 2, value: 'Local B' }, { id: 3, value: 'Outstation A' }, { id: 4, value: 'Outstation B' }, { id: 5, value: 'Marketing' }]
  RETURN_TYPE: any = [{ id: 1, value: 'Highly Possible ' }, { id: 2, value: 'Normal Possibility' }, { id: 3, value: 'Low Possibility' }]

  config: any;
  active_day = new Date()
  dispatch_day = new Date()
  selected_date = new Date();
  isLoading;
  isFollowUp = false
  isExpand = false
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Shop Detail // Shop
  Shop: any;
  detail_shop() {
    this.Shop = this.shopService.shop

  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Customers Detail // Customers
  Customers: any;
  list_customers() {
    this.shopService.customer_list().subscribe(
      resp => {
        this.isLoading = false
        this.Customers = resp
        console.log(resp);
      })
  }

  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factors Detail // Factors
  Factors: any;
  Invoice: any = new Object();
  list_invoices() {
    this.Invoice.filter_type = '1'
    this.Invoice.filter_date_one = this.DateTimetoDay(this.active_day)
    this.shopService.invoice_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.config = this.shopService.config
        this.rerender()
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ invoice ]
  Factor: any = new Object()
  create_factor(form: NgForm) {

    this.Factor.return_types = form.value.return_types
    this.Factor.invoice_refernce_no = form.value.invoice_refernce_no
    this.Factor.invoice_balance = form.value.invoice_balance
    this.Factor.return_types = 1
    form.value.follow_up_comment == undefined ? this.Factor.follow_up_comment = ' - ' : console.log('Filled follow_up_comment');

    this.Factor.next_follow_up = this.DateTimetoDay(this.selected_date) + 'T00:00'
    this.Factor.dispatch_date = this.DateTimetoDay(this.dispatch_day) + 'T00:00'
    console.log(this.Factor);
    console.log(this.selected_date);

    this.shopService.invoice_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_invoices()
        this.Factor = new Object()
        if (this.isFollowUp) {
          this.disclose(this.Invoice)
          this.isFollowUp = false
        }
        this.Invoice = new Object();
        $('#create-factor-modal').modal('hide');
        form.reset()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ invoice ]
  edit_factor(form: NgForm) {


    // this.Factor.return_types = this.Factor.return_types
    // this.Factor.return_types = 1
    // this.Factor.dispatch_date = this.DateTimetoDay(this.Factor.dispatch_date) + 'T00:00'

    this.Factor.customer = this.Factor.customer_id
    this.Factor.invoice_balance = form.value.invoice_balance
    form.value.follow_up_comment == undefined ? this.Factor.follow_up_comment = ' - ' : console.log('Filled follow_up_comment');
    this.Factor.next_follow_up = this.DateTimetoDay(this.selected_date) + 'T00:00'
    this.Factor.invoice_refernce_no = this.Factor.invoice_refernce_no
    console.log(this.Factor);

    this.shopService.invoice_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_invoices()
        this.Factor = new Object()
        this.disclose(this.Invoice)
        this.isFollowUp = false
        this.Invoice = new Object();
        $('#edit-factor-modal').modal('hide');
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ invoice -  ]
  delete_factor(ele: any) {
    this.Factor.customer_id = ele.customer_id
    this.shopService.invoice_delete(this.Factor).subscribe(
      resp => {
        this.list_invoices()
      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  show_edit(ele: any) {
    this.Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  togglePanels() {
    if (this.isExpand) { this.accordion.closeAll() }
    else { this.accordion.openAll() }
    this.isExpand = !this.isExpand
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Factors Functions
  next_followup(ele) {
    this.Factor = ele
    $('#edit-factor-modal').modal('show')
    this.isFollowUp = true
    this.Invoice = ele
  }
  disclose(ele) {
    this.Factor = new Object()
    this.Factor.customer = ele.customer_id
    this.Factor.invoice_id = ele.invoice_id
    this.Factor.dislosed_state = true
    this.shopService.invoice_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_invoices()
        this.Factor = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Timer Functions
  // Input : Raw DateTime // Output : dd-mm-yy
  DateTimetoDay(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  isCustomerSelected: any = false
  Customer: any = new Object();
  show_detail(ele: any) {
    console.log(ele);
    Storage.set({ key: 'customer_id', value: ele.customer_id });
    this.Customer.customer_id = ele.customer_id
    this.ngOnInit()
    this.isCustomerSelected = true
    // this.isCustomerSelected = false
    this.detail_customer()
    $('#customer-detail-modal').modal('show')
  }

  detail_customer() {
    this.shopService.customer_detail(this.Customer).subscribe(
      // this.Customer = new Object()
      resp => {
        this.Customer = resp
        console.log(resp);
        this.Customer["leads"] = this.Customer["leads"].reverse()
        this.Customer["invoices"] = this.Customer["invoices"].reverse()
        this.Customer["tasks"] = this.Customer["tasks"].reverse()
        this.isLoading = false
      })
  }
  add_lead(){
    $('#customer-detail-modal').modal('hide')
    $('#create-lead-modal').modal('show');
    this.Factor.customer = this.Customer.customer_id
    // this.router.navigate(['/dashboard/lead-management'], {state: {flag: 1 , customer: this.Customer }});
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    responsive: true,
    ordering: false
  };
  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  update_config() {
    Storage.set({ key: 'config', value: JSON.stringify(this.config) });
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
}
