import { ShopService } from './../../backend/shop.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-approval-management',
  templateUrl: './approval-management.component.html',
  styleUrls: ['./approval-management.component.scss']
})
export class ApprovalManagementComponent implements OnInit {


  constructor(private shopService: ShopService, private router: Router) { }

  ngOnInit(): void {
    this.isLoading = true
    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  active_day: any = new Date()

  PRIORITY: any = [{ id: 1, value: 'High' }, { id: 2, value: 'Normal' }, { id: 3, value: 'Low' }]
  CUSTOMER_FROM: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Local A' }, { id: 2, value: 'Local B' }, { id: 3, value: 'Outstation A' }, { id: 4, value: 'Outstation B' }, { id: 5, value: 'Marketing' }]

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products [ vendor ]
  isLoading;
  Factor: any = new Object()
  Factors: any = [];
  Activities: any = [];
  Leads: any = [];
  Invoices: any = [];
  Tasks: any = [];
  list_factor() {
    this.isLoading = true
    this.shopService.approval_list().subscribe(
      resp => {

        this.Factors = resp
        console.log(resp);
        console.log(this.Factors[0]['invoices']);
        this.Leads = this.Factors[0]['leads']
        this.Invoices = this.Factors[0]['invoices']
        this.Tasks = this.Factors[0]['tasks']
        // this.rerender()
        console.log(this.Leads, this.Invoices, this.Tasks);
        this.isLoading = false

      }
    )
    this.shopService.activity_list().subscribe(
      resp => {
        console.log(resp);
        this.Activities = resp
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  lead_disclose(ele) {
    this.Factor = ele
    this.Factor.approval_state = true
    this.Factor.customer = ele.customer_id
    console.log(this.Factor);
    this.Factor.next_follow_up = this.DateTimetoDay(this.Factor.next_follow_up) + 'T00:00'
    this.Factor.follow_up_comment = ele.follow_up_comment


    this.shopService.lead_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  invoice_disclose(ele) {
    this.Factor = ele
    this.Factor.customer = ele.customer_id
    this.Factor.approval_state = true
    this.Factor.next_follow_up = this.DateTimetoDay(this.Factor.next_follow_up) + 'T00:00'
    this.Factor.follow_up_comment = ele.follow_up_comment

    this.shopService.invoice_edit(this.Factor).subscribe(
      resp => {
        console.log(this.Factor)
        this.list_factor()
        this.Factor = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  task_disclose(ele) {
    this.Factor = ele
    this.Factor.approval_state = true
    this.shopService.task_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  lead_disapprove(ele) {
    this.Factor = ele
    this.Factor.dislosed_state = true
    this.Factor.customer = ele.customer_id
    console.log(this.Factor);
    this.Factor.next_follow_up = this.DateTimetoDay(this.Factor.next_follow_up) + 'T00:00'
    this.shopService.lead_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  invoice_disapprove(ele) {
    this.Factor = ele
    this.Factor.customer = ele.customer_id
    this.Factor.dislosed_state = true
    this.Factor.next_follow_up = this.DateTimetoDay(this.Factor.next_follow_up) + 'T00:00'

    this.shopService.invoice_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  task_disapprove(ele) {
    this.Factor = ele
    this.Factor.dislosed_state = true
    this.shopService.task_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  onDateChange(e) {
    console.log(this.active_day);
    this.active_day = e.target.value
    this.list_factor()
  }

  isCustomerSelected: any = false
  show_detail(ele: any) {
    console.log(ele);
    this.isCustomerSelected = true
    Storage.set({ key: 'customer_id', value: ele.customer_id });
    $('#customer-detail-modal').modal('show')
    // this.isCustomerSelected = false
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Timer Functions
  // Input : Raw DateTime // Output : dd-mm-yy
  DateTimetoDay(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // @ViewChild(DataTableDirective, { static: false })
  // dtEle: DataTableDirective;
  // dtTrigger: Subject<any> = new Subject<any>();
  // dtOptions: DataTables.Settings = {
  //   // responsive: true,
  //   // ordering: false
  // };

  // // DataTables Functions
  // ngAfterViewInit(): void {
  //   this.dtTrigger.next();
  // }

  // ngOnDestroy(): void {
  //   // Do not forget to unsubscribe the event
  //   this.dtTrigger.unsubscribe();
  // }

  // rerender(): void {
  //   this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
  //     // Destroy the table first
  //     dtInstance.destroy();
  //     // Call the dtTrigger to rerender again
  //     this.dtTrigger.next();
  //   });
  // }
  /////////////////////////////////////////////////////////////////////////////////////////////


}
