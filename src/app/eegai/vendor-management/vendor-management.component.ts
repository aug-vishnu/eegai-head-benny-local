import { ShopService } from './../../backend/shop.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Component({
  selector: 'app-vendor-management',
  templateUrl: './vendor-management.component.html',
  styleUrls: ['./vendor-management.component.scss']
})
export class VendorManagementComponent implements OnInit {


  constructor(private shopService: ShopService, private router: Router) { }

  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    this.list_industries()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Variables
  isLoading: any = false
  CUSTOMER_FROM: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  VENDOR_TYPE: any = [{ id: 1, value: 'Pre Printing' }, { id: 2, value: 'Post Printing' }]
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Local A' }, { id: 2, value: 'Local B' }, { id: 3, value: 'Outstation A' }, { id: 4, value: 'Outstation B' }, { id: 5, value: 'Marketing' }]
  config: any;
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products [ vendor ]
  Factors: any = [];
  list_factor() {
    this.isLoading = true
    this.shopService.vendor_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.config = this.shopService.config
        this.rerender()
      }
    )
  }

  Industries: any = [];
  selectedIndustry: any = null;
  list_industries() {
    this.shopService.customer_industry_list().subscribe(
      resp => {
        this.Industries = resp
        console.log(resp);
        this.isLoading = false
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ vendor ]
  Factor: any = new Object()
  create_factor(form: NgForm) {
    console.log(form.value);
    console.log(this.Factor);
    form.value.vendor_feedback == undefined ? this.Factor.vendor_feedback = ' - ' : console.log('Filled vendor_feedback');
    this.shopService.vendor_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        $('#create-factor-modal').modal('hide');
        form.reset()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ vendor ]

  edit_factor(form: NgForm) {
    console.log(this.Factor);
    console.log(form.value);
    this.Factor.shop_industry = this.selectedIndustry
    // this.Factor.vendor_type = form.value.vendor_type
    // this.Factor.vendor_from = form.value.vendor_from
    this.Factor.vendor_from = 1
    this.Factor.vendor_type = 2
    form.value.calendar_eligible == undefined ? this.Factor.calendar_eligible = false : this.Factor.calendar_eligible = true

    this.shopService.vendor_edit(this.Factor).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ vendor -  ]
  delete_factor(ele: any) {
    this.Factor.vendor_id = ele.vendor_id
    this.shopService.vendor_delete(this.Factor).subscribe(
      resp => {
        this.list_factor()
        $('#edit-factor-modal').modal('hide');
      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  show_edit(ele: any) {
    this.Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    // responsive: true,
    pageLength: 100,
    // ordering: false
  };

  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  update_config() {
    Storage.set({ key: 'config', value: JSON.stringify(this.config) });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


}
