import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotepadManagementComponent } from './notepad-management.component';

describe('NotepadManagementComponent', () => {
  let component: NotepadManagementComponent;
  let fixture: ComponentFixture<NotepadManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotepadManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotepadManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
