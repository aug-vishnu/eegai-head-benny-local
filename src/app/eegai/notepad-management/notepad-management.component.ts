import { NgForm } from '@angular/forms';
import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Component, OnInit, ViewChild } from '@angular/core';
declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

import { MatAccordion } from '@angular/material/expansion';

@Component({
  selector: 'app-notepad-management',
  templateUrl: './notepad-management.component.html',
  styleUrls: ['./notepad-management.component.scss']
})
export class NotepadManagementComponent implements OnInit {


  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) { }

  ngOnInit() {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factors()
    /////////////////////////////////////////////////////////////////////////////////////////////
  }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Date Operations
  PRIORITY: any = [{ id: 1, value: '1 - High' }, { id: 2, value: '2 - Normal' }, { id: 3, value: '3 - Low' }]
  CUSTOMER_FROM: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Local A' }, { id: 2, value: 'Local B' }, { id: 3, value: 'Outstation A' }, { id: 4, value: 'Outstation B' }, { id: 5, value: 'Marketing' }]

  selectedPriority: any;

  max_today = new Date()
  selected_date = new Date();
  isLoading = false
  isFollowUp = false
  isStateEdit = false
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Notes Detail // Task
  Notes: any;
  Machines: any;
  Task: any = new Object();

  list_factors() {
    this.shopService.notepad_list().subscribe(
      resp => {
        this.Notes = resp
        console.log(resp);
        this.isLoading = false
        this.rerender()
      })

  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ journal ]
  Factor: any = new Object()
  create_factor(form: NgForm) {
    this.shopService.notepad_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factors()
        this.Factor = new Object()
        $('#create-factor-modal').modal('hide');
        form.reset()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ journal ]
  edit_factor(form: NgForm) {
    console.log(this.Factor);
    this.shopService.notepad_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factors()
        this.Factor = new Object()
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ journal -  ]
  delete_factor(ele: any) {
    this.Factor.notepad_id = ele.notepad_id
    this.shopService.notepad_delete(this.Factor).subscribe(
      resp => {
        this.list_factors()
      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  show_edit(ele: any) {
    this.Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Timer Functions
  // Input : Raw DateTime // Output : dd-mm-yy
  formatDateforPicker(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    // responsive: true,
    // ordering: false
  };


  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  // togglePanels() {
  //   if (this.isExpand) { this.accordion.closeAll() }
  //   else { this.accordion.openAll() }
  //   this.isExpand = !this.isExpand
  // }
  /////////////////////////////////////////////////////////////////////////////////////////////


}
