import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

import { NgForm } from '@angular/forms';
import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Component({
  selector: 'app-lead-management',
  templateUrl: './lead-management.component.html',
  styleUrls: ['./lead-management.component.scss']
})
export class LeadManagementComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) {
    console.log(this.router.getCurrentNavigation()?.extras?.state);
    var state = this.router.getCurrentNavigation()?.extras?.state
    console.log(state);


    if(state?.flag == 1){
      this.Factor = state
      console.log(state.customer);
      // $('#create-factor-modal').modal('show')
    }

  }

  ngOnInit() {
    console.log(this.Factor?.flag);
    $(".modal-backdrop").remove();
    if(this.Factor?.flag == 1){
      $('#create-factor-modal').modal('show')
    }
    this.isLoading = true
    this.config = this.shopService.config

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.detail_shop()
    this.list_leads()
    this.list_customers()
    /////////////////////////////////////////////////////////////////////////////////////////////
  }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Date Operations
  PRIORITY: any = [{ id: 1, value: 'High' }, { id: 2, value: 'Normal' }, { id: 3, value: 'Low' }]
  CUSTOMER_FROM: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Local A' }, { id: 2, value: 'Local B' }, { id: 3, value: 'Outstation A' }, { id: 4, value: 'Outstation B' }, { id: 5, value: 'Marketing' }]

  config: any;
  active_day = new Date()
  selected_date = new Date();
  isLoading = false
  isExpand = false
  isFollowUp = false
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Shop Detail // Shop
  Shop: any;
  detail_shop() {
    this.Shop = this.shopService.shop
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Customers Detail // Customers
  Customers: any;
  list_customers() {
    this.shopService.customer_list().subscribe(
      resp => {
        this.Customers = resp
        console.log(resp);
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factors Detail // Factors
  Factors: any;
  Lead: any = new Object();
  list_leads() {
    this.Lead.filter_type = '1'
    this.Lead.filter_date_one = this.DateTimetoDay(this.active_day)
    console.log(this.Lead);

    this.shopService.lead_filter(this.Lead).subscribe(
      resp => {
        this.Factors = resp
        this.isLoading = false
        console.log(resp);
        this.rerender()

      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ lead ]
  Factor: any = new Object()
  create_factor(form: NgForm) {

    this.Factor.lead_priority = 2 // Here
    this.Factor.next_follow_up = this.DateTimetoDay(this.selected_date) + 'T00:00'
    console.log(this.Factor);
    console.log(this.selected_date);

    this.shopService.lead_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_leads()
        this.Factor = new Object()
        if (this.isFollowUp) {
          this.disclose(this.Lead)
          this.isFollowUp = false
        }
        this.Lead = new Object();
        $('#create-factor-modal').modal('hide');
        form.reset()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ lead ]
  edit_factor(form: NgForm) {
    console.log(this.Factor);
    this.shopService.lead_edit(this.Factor).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_leads()
        this.Factor = new Object()
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  next_followup(ele) {
    $('#create-factor-modal').modal('show')
    this.isFollowUp = true
    this.Factor.customer = ele.customer_id
    this.Lead = ele
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ lead -  ]
  delete_factor(ele: any) {
    this.Factor.customer_id = ele.customer_id
    this.shopService.lead_delete(this.Factor).subscribe(
      resp => {
        this.list_leads()
      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  onDateChange(e) {
    console.log(this.active_day);
    this.active_day = e.target.value
    this.list_leads()
  }
  togglePanels() {
    if (this.isExpand) { this.accordion.closeAll() }
    else { this.accordion.openAll() }
    this.isExpand = !this.isExpand
  }
  show_edit(ele: any) {
    this.Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Factors Functions

  disclose(ele) {
    console.log(ele);

    this.Factor = new Object()
    this.Factor.lead_id = ele.lead_id
    this.Factor.customer = ele.customer_id
    this.Factor.dislosed_state = true
    this.shopService.lead_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_leads()
        this.Factor = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Timer Functions
  // Input : Raw DateTime // Output : dd-mm-yy
  DateTimetoDay(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  isCustomerSelected: any = false
  Customer: any = new Object();
  show_detail(ele: any) {
    console.log(ele);
    Storage.set({ key: 'customer_id', value: ele.customer_id });
    this.Customer.customer_id = ele.customer_id
    this.ngOnInit()
    this.isCustomerSelected = true
    // this.isCustomerSelected = false
    this.detail_customer()
    $('#customer-detail-modal').modal('show')
  }

  detail_customer() {
    this.shopService.customer_detail(this.Customer).subscribe(
      // this.Customer = new Object()
      resp => {
        this.Customer = resp
        console.log(resp);
        this.Customer["leads"] = this.Customer["leads"].reverse()
        this.Customer["invoices"] = this.Customer["invoices"].reverse()
        this.Customer["tasks"] = this.Customer["tasks"].reverse()
        this.isLoading = false
      })
  }
  add_lead(){
    $('#customer-detail-modal').modal('hide')
    $('#create-lead-modal').modal('show');
    this.Lead.customer = this.Customer.customer_id
    // this.router.navigate(['/dashboard/lead-management'], {state: {flag: 1 , customer: this.Customer }});
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    responsive: true,
    ordering: false
  };

  ngAfterViewInit(): void {
    this.dtTrigger.next();

    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
        const that = this;
        $('input', this.footer()).on('keyup change', function () {
          if (that.search() !== this['value']) {
            that
              .search(this['value'])
              .draw();
          }
        });
      });
    });
  }

  // // DataTables Functions
  // ngAfterViewInit(): void {
  //   this.dtTrigger.next();
  // }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  update_config() {
    Storage.set({ key: 'config', value: JSON.stringify(this.config) });
  }

  /////////////////////////////////////////////////////////////////////////////////////////////

}
