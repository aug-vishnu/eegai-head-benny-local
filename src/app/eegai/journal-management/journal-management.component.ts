import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

import { NgForm } from '@angular/forms';
import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
declare var $: any;


@Component({
  selector: 'app-journal-management',
  templateUrl: './journal-management.component.html',
  styleUrls: ['./journal-management.component.scss']
})
export class JournalManagementComponent implements OnInit {

  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) { }

  ngOnInit() {

    this.isLoading = true
    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factors()
    /////////////////////////////////////////////////////////////////////////////////////////////
  }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Date Operations
  PRIORITY: any = [{ id: 1, value: 'High' }, { id: 2, value: 'Normal' }, { id: 3, value: 'Low' }]
  CUSTOMER_FROM: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Local A' }, { id: 2, value: 'Local B' }, { id: 3, value: 'Outstation A' }, { id: 4, value: 'Outstation B' }, { id: 5, value: 'Marketing' }]

  selectedPriority: any;

  max_today = new Date()
  selected_date = new Date();
  isLoading;
  isFollowUp = false
  isStateEdit = false
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Tasks Detail // Task
  Tasks: any;
  Machines: any;
  Task: any = new Object();

  list_factors() {
    this.shopService.journal_list().subscribe(
      resp => {
        this.Tasks = resp
        console.log(resp);
        this.isLoading = false
        console.log("adfddddddddddddd");
        this.rerender()
        if (this.isLoading) {
          this.Factor = this.Tasks[0]
          $('#edit-factor-modal').modal('show');
        }
      })
    this.shopService.machine_list().subscribe(
      resp => {
        this.Machines = resp
        console.log(resp);
        this.isLoading = false
      })

  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ journal ]
  Factor: any = new Object()
  create_factor(form: NgForm) {
    this.Factor.follow_up = this.selected_date
    form.value.work_free == undefined ? this.Factor.work_free = false : this.Factor.work_free = true

    // console.log(form.value);
    console.log(this.Factor);
    this.shopService.journal_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        if (resp['data'].work_free == false) {
          this.isLoading = true
        }
        this.list_factors()
        this.Factor = new Object()
        $('#create-factor-modal').modal('hide');
        form.reset()
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ journal ]
  edit_factor(form: NgForm) {
    console.log(this.Factor);
    for (let index = 0; index < this.Factor['responses'].length; index++) {
      const element = this.Factor['responses'][index];
      console.log(element);
      this.shopService.journal_response_edit(element).subscribe(
        resp => {
          console.log(resp)
          this.list_factors()
          this.Factor = new Object()
          $('#edit-factor-modal').modal('hide');
        });
    }
  }

  update_response(response) {
    console.log(this.Factor);
    this.shopService.journal_response_edit(response).subscribe(
      resp => {
        console.log(resp)
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ journal -  ]
  delete_factor(ele: any) {
    this.Factor.journal_id = ele.journal_id
    this.shopService.journal_delete(this.Factor).subscribe(
      resp => {
        this.list_factors()
      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Status [ task ]
  Machine: any = new Object()
  add_machine(form: NgForm) {

    this.shopService.machine_create(form.value).subscribe(
      resp => {
        console.log(resp)
        this.list_factors()
        $('#create-machine-modal').modal('hide');
        this.Machine = new Object()
      },
    );
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_machine(ele: any) {
    this.Machine.machine_id = ele.machine_id
    this.shopService.machine_delete(this.Machine).subscribe(
      resp => {
        this.list_factors()
        $('#create-machine-modal').modal('hide');

      },
    )
  }

  // End Create Status
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  show_edit(ele: any) {
    this.Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Tasks Functions
  next_followup(ele) {
    $('#create-factor-modal').modal('show')
    this.isFollowUp = true
    this.Task = ele

  }
  disclose(ele) {
    this.Factor = new Object()
    this.Factor.journal_id = ele.journal_id
    this.Factor.dislosed_state = true
    this.shopService.journal_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factors()
        this.Factor = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Timer Functions
  // Input : Raw DateTime // Output : dd-mm-yy
  formatDateforPicker(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    // responsive: true,
    ordering: false,
    pageLength: 100,
  };


  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  // togglePanels() {
  //   if (this.isExpand) { this.accordion.closeAll() }
  //   else { this.accordion.openAll() }
  //   this.isExpand = !this.isExpand
  // }
  /////////////////////////////////////////////////////////////////////////////////////////////

}
